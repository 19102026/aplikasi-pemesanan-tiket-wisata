<?php

/**
 * File config.php ini berisi konfigurasi PATH dan database
 */

// Mendefinisikan alamat url aplikasi
define('URL', 'http://localhost/serkom_lutfir/');

// Mendefinisikan koneksi database
// Host database
define('DB_HOST', 'localhost');

// Nama database
define('DB_NAME', 'db_wisata');

// Nama pengguna database
define('DB_USER', 'root');

// Kata sandi database
define('DB_PASS', '');
