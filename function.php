<?php
// Mengambil file model
include_once __DIR__ . "/models/tiket.php";

// Untuk mengambil komponen dari folder component
function load_component($component)
{
    include_once __DIR__ . '/component/' . $component . '.php';
}

// Untuk mengambil data dari request
function request($parameter)
{
    return isset($_REQUEST[$parameter]) ? $_REQUEST[$parameter] : null;
}

// Untuk mengecek apakah metode request adalah POST
function is_post()
{
    return $_SERVER['REQUEST_METHOD'] === 'POST';
}

// Prosedur untuk mencetak judul halaman
function judul($title)
{
    echo "<h4>$title</h4>";
}

// Untuk Memuat halaman 
function load_view($view, $data = [])
{
    include_once __DIR__ .'/' . $view . '.php';
    die();
}

// Untuk mengalihkan halaman
function redirect($page)
{
    header("Location: $page");
    die();
}

function pemesanan_simpan()
{
    // tangkap data dari request
    $nama_lengkap = request('nama_lengkap');
    $nomor_identitas = request('nomor_identitas');
    $nomor_hp  = request('nomor_hp');
    $wisata = request('wisata');
    $tgl_kunjungan = request('tgl_kunjungan');
    $jumlah_pengunjung_dewasa = request('jumlah_pengunjung_dewasa');
    $jumlah_pengunjung_anak = request('jumlah_pengunjung_anak');
    $harga_tiket = request('harga_tiket');
    $total_bayar = request('total_bayar');

    // Simpan data ke database
    save_pemesanan(
        $nama_lengkap, 
        $nomor_identitas, 
        $nomor_hp, 
        $wisata, 
        $tgl_kunjungan, 
        $jumlah_pengunjung_dewasa, 
        $jumlah_pengunjung_anak,
        $harga_tiket, 
        $total_bayar
    );

    // Mengembalikan nilai
    return [
        'nama_lengkap' => $nama_lengkap,
        'nomor_identitas' => $nomor_identitas,
        'nomor_hp' => $nomor_hp,
        'wisata' => $wisata,
        'tgl_kunjungan' => $tgl_kunjungan,
        'jumlah_pengunjung_dewasa' => $jumlah_pengunjung_dewasa,
        'jumlah_pengunjung_anak' => $jumlah_pengunjung_anak,
        'harga_tiket' => $harga_tiket,
        'total_bayar' => $total_bayar
    ];
}