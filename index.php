<!-- Memanggil fungsi yang telah di definisikan sebelumnya -->
<?php include_once 'function.php'; ?>

<!-- Mengambil komponen header -->
<?php load_component('header'); ?>

<div class="container">

    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/8NIqWEyxjM4"
                    title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
                <div class="carousel-caption d-none d-md-block">
                    <h5>Museum Gedung Sate</h5>
                </div>
            </div>
            <div class="carousel-item">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/A47K2PKnCCQ"
                    title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
                <div class="carousel-caption d-none d-md-block">
                    <h5>Pantai Pandawa</h5>
                </div>
            </div>
            <div class="carousel-item">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/Q84LIJSNTQo"
                    title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
                <div class="carousel-caption d-none d-md-block">
                    <h5>Taman Nasional Bantimurung Bulusaraung</h5>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-target="#carouselExampleCaptions" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-target="#carouselExampleCaptions" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </button>
    </div>

    <!-- Mencetak judul dengan fungsi prosedur -->
    <?= judul("Nama Wisata") ?>
    <br>
    <div class="row">
        <!-- Looping data -->
        <?php for ($i = 0; $i < count(get_wisata()); $i++) { ?>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body p-0">
                    <img width="100%" height="300px" src="<?= URL ?>/images/<?= get_wisata()[$i]['foto'] ?>" alt="">
                </div>
                <div class="card-footer text-center">
                    <b>
                        <?= get_wisata()[$i]['nama'] ?>
                    </b>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>

<!-- Mengambil komponen footer -->
<?php load_component('footer'); ?>