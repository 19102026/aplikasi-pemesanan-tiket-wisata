<!-- Memanggil fungsi yang telah di definisikan sebelumnya -->
<?php include_once 'function.php'; ?>

<!-- Mengambil komponen header -->
<?php load_component('header'); ?>

<?php
if (is_post()) {
    // Simpan data yang dikirimkan ke fungsi save_pemesanan
    $result = pemesanan_simpan();

    // Jika data berhasil disimpan, maka tampilkan kedalam view konfirmasi_pemesanan.php
    return load_view('konfirmasi_pemesanan', $result);
}
?>

<div class="container">
    <!-- Mencetak judul dengan fungsi prosedur -->
    <?= judul("Form Pemesanan Tiket Wisata") ?>
    <br>

    <!-- Form Pemesanan -->
    <form action="" id="formPesanTiket" method="post">
        <div class="form-group row">
            <label for="nama_lengkap" class="col-sm-4 col-form-label">Nama Lengkap</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" required name="nama_lengkap" id="nama_lengkap">
            </div>
        </div>
        <div class="form-group row">
            <label for="nomor_identitas" class="col-sm-4 col-form-label">Nomor Identitas</label>
            <div class="col-sm-8">
                <input type="string" maxlength="16" class="form-control" required name="nomor_identitas"
                    id="nomor_identitas">
            </div>
        </div>
        <div class="form-group row">
            <label for="nomor_hp" class="col-sm-4 col-form-label">Nomor Hp</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" required name="nomor_hp" id="nomor_hp">
            </div>
        </div>
        <div class="form-group row">
            <label for="wisata" class="col-sm-4 col-form-label">Tempat Wisata</label>
            <div class="col-sm-8">
                <select class="form-control" name="wisata" required id="wisata">
                    <option selected disabled>Pilih</option>

                    <?php foreach (nama_kelas() as $key => $value) { ?>
                    <option value="<?= $key ?>">
                        <?= $value ?>
                    </option>
                    <?php } ?>

                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="tgl_kunjungan" class="col-sm-4 col-form-label">Tanggal Kunjungan</label>
            <div class="col-sm-8">
                <input type="date" class="form-control" required name="tgl_kunjungan" id="tgl_kunjungan">
            </div>
        </div>
        <div class="form-group row">
            <label for="jumlah_pengunjung_dewasa" class="col-sm-4 col-form-label">Pengunjung Dewasa</label>
            <div class="col-sm-8">
                <input type="number" class="form-control" value="0" required name="jumlah_pengunjung_dewasa"
                    id="jumlah_pengunjung_dewasa">
            </div>
        </div>
        <div class="form-group row">
            <label for="jumlah_pengunjung_anak" class="col-sm-4 col-form-label">Pengunjung Anak (Usia < 12
                    tahun)</label>
                    <div class="col-sm-8">
                        <input type="number" class="form-control" value="0" required name="jumlah_pengunjung_anak"
                            id="jumlah_pengunjung_anak">
                    </div>
        </div>

        <!-- Menampilkan Harga tiket dan Total bayar -->
        <div class="form-group row">
            <label for="jumlah_pengunjung_dewasa" class="col-sm-4 col-form-label">Harga Tiket</label>
            <div class="col-sm-8">
                <span id="hargaTiket">Rp. 0</span>
                <input value="0" type="hidden" name="harga_tiket" id="harga_tiket" hidden>
            </div>
        </div>
        <div class="form-group row">
            <label for="jumlah_pengunjung_dewasa" class="col-sm-4 col-form-label">Total bayar</label>
            <div class="col-sm-8">
                <span id="totalBayar">Rp. 0</span>
                <input value="0" type="hidden" name="total_bayar" id="total_bayar" hidden>
            </div>
        </div>

        <div class="form-group">
            <input type="checkbox" class="mt-3" id="agree" name="agree" require>
            Saya dan/atau rombongan telah membaca, memahami dan setuju berdasarkan syarat dan ketentuan yang telah
            ditetapkan
        </div>

        <br>

        <!-- Button -->
        <div class="row">
            <div class="col-lg-4">
                <button type="button" id="totalBayarBtn" class="btn w-100 mb-1 btn-primary">
                    Hitung Total Bayar
                </button>
            </div>
            <div class="col-lg-4">
                <button type="button" id="pesanTiketBtn" class="btn w-100 mb-1 btn-primary">
                    Pesan Tiket
                </button>
            </div>
            <div class="col-lg-4">
                <a href="<?= URL ?>" type="button" class="btn mb-1 w-100 btn-primary">
                    Cancel
                </a>
            </div>
        </div>
    </form>
</div>

<script>
/* Fungsi formatRupiah */
function formatRupiah(bilangan) {

    var reverse = bilangan.toString().split('').reverse().join(''),
        ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join('.').split('').reverse().join('');

    return "Rp. " + ribuan;
}

// array untuk menyimpan data harga tempat wisata
var harga_wisata = []
<?php for ($i = 0; $i < count(get_wisata()); $i++) { ?>
harga_wisata[<?= get_wisata()[$i]['id'] ?>] = <?= get_wisata()[$i]['harga'] ?>;
<?php } ?>

// jQuery untuk button Hitung Total Bayar
$('#totalBayarBtn').click(function() {

    var jumlah_pengunjung_anak = $('#jumlah_pengunjung_anak').val();
    var jumlah_pengunjung_dewasa = $('#jumlah_pengunjung_dewasa').val();
    var nomor_identitas = $('#nomor_identitas').val();
    var wisata = $('#wisata').val();

    // Jika nomor identitas tidak 16 digit
    if (nomor_identitas.length != 16) {
        // tampilkan pesan error
        alert('No Identitas Harus 16 Digit');
        return;
    }

    // Jika tempat wisata tidak dipilih
    if (wisata == null) {
        // tampilkan pesan error
        alert('Harap Pilih Tempat Wisata');
        return;
    }

    // Jika Jumlah Pengunjung Anak tidak kosong
    if (jumlah_pengunjung_anak != 0 && jumlah_pengunjung_anak != null) {
        // Hitung diskon 50%
        var diskon = (Number(harga_wisata[wisata]) * 50) / 100;

        // Mengambil data harga wisata ke variable harga tiket
        var harga_tiket = Number(harga_wisata[wisata]);

        // Set harga tiket yang tersembunyi 
        $("#harga_tiket").val(harga_tiket);

        // Tampilkan harga tike
        $('#hargaTiket').text(formatRupiah(harga_tiket));

        // Hitung harga tiket
        var harga_tiket = Number(harga_wisata[wisata]) - diskon;

        // Hitung total bayar
        var total_bayar = (harga_tiket * Number(jumlah_pengunjung_anak)) + (Number(jumlah_pengunjung_dewasa)) *
            Number(harga_wisata[wisata]);

        // Set pada total bayar yang tersembunyi
        $("#total_bayar").val(total_bayar);

        // Tampilkan total bayar
        $('#totalBayar').text(formatRupiah(total_bayar));
    } else {
        // Mengambil data harga wisata ke variable harga tiket
        var harga_tiket = Number(harga_wisata[wisata]);

        // Set harga tiket yang tersembunyi
        $("#harga_tiket").val(harga_tiket);

        // Tampilkan harga tiket
        $('#hargaTiket').text(formatRupiah(harga_tiket));

        // Hitung total bayar
        var total_bayar = harga_tiket * (Number(jumlah_pengunjung_anak) + Number(jumlah_pengunjung_dewasa));

        // Set pada total bayar yang tersembunyi
        $("#total_bayar").val(total_bayar);

        // Tampilkan total bayar
        $('#totalBayar').text(formatRupiah(total_bayar));
    }
});

// jQuery untuk button Pesan Tiket
$('#pesanTiketBtn').click(function() {
    var nomor_identitas = $('#nomor_identitas').val();
    var total_bayar = $('#total_bayar').val();

    // Jika nomor identitas tidak 16 digit
    if (nomor_identitas.length != 16) {
        // tampilkan pesan error
        alert('No Identitas Harus 16 Digit');
        return;
    }

    // jika total bayar adalah 0 atau NaN
    if (total_bayar == 0) {
        // tampilkan pesan error
        alert('Harap Hitung Total Bayar Terlebih Dahulu');
        return;
    }

    $('#formPesanTiket').submit();
});
</script>

<!-- Mengambil komponen footer -->
<?php load_component('footer'); ?>