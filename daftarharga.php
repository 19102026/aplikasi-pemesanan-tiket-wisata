<!-- Memanggil fungsi yang telah di definisikan sebelumnya -->
<?php include_once 'function.php';?>

<!-- Mengambil komponen header -->
<?php load_component('header');?>

<div class="container">
    <!-- Mencetak judul dengan fungsu prosedur -->
    <?= judul("Daftar Harga") ?>
    <br>
    <div class="table-responsive">
        <!-- Membuat tabel daftar harga -->
        <table class="table">
            <thead>
                <tr>
                    <td>
                        No
                    </td>
                    <td>
                        Nama
                    </td>
                    <td>
                        Harga
                    </td>
                </tr>
            </thead>
            <tbody>
                <!-- Looping data -->
                <?php for ($i = 0; $i < count(get_wisata()); $i++) { ?>
                <tr>
                    <td>
                        <?= $i + 1 ?>
                    </td>
                    <td>
                        <?= get_wisata()[$i]['nama'] ?>
                    </td>
                    <td>
                        <?= get_wisata()[$i]['harga'] ?>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Mengambil komponen footer -->
<?php load_component('footer');?>