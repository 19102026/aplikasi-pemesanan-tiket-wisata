<!-- Memanggil fungsi yang telah di definisikan sebelumnya -->
<?php include_once 'function.php'; ?>

<!-- Mengambil komponen header -->
<?php load_component('header'); ?>

<?php
if (empty($data)) {
    return redirect("pemesanan.php");
}
?>

<div class="container">
    <!-- Mencetak judul dengan fungsi prosedur -->
    <?= judul("Konfirmasi Pemesanan") ?>
    <br>

    <!-- Memanggil data dari pemesanan.php -->
    <table class="w-100">
        <tr>
            <td>
                Nama Lengkap
            </td>
            <td style="width: 10px" class="text-center">:</td>
            <td>
                <?= $data['nama_lengkap'] ?>
            </td>
        </tr>
        <tr>
            <td>
                Nomor Identitas
            </td>
            <td style="width: 10px" class="text-center">:</td>
            <td>
                <?= $data['nomor_identitas'] ?>
            </td>
        </tr>
        <tr>
            <td>
                Nomor HP
            </td>
            <td style="width: 10px" class="text-center">:</td>
            <td>
                <?= $data['nomor_hp'] ?>
            </td>
        </tr>
        <tr>
            <td>
                Tempat Wisata
            </td>
            <td style="width: 10px" class="text-center">:</td>
            <td>
                <?= wisata_by_id($data['wisata']) ?>
            </td>
        </tr>
        <tr>
            <td>
                Jadwal Kunjungan
            </td>
            <td style="width: 10px" class="text-center">:</td>
            <td>
                <?= date('d-m-Y', strtotime($data['tgl_kunjungan'])) ?>
            </td>
        </tr>
        <tr>
            <td>
                Pengunjung Dewasa
            </td>
            <td style="width: 10px" class="text-center">:</td>
            <td>
                <?= $data['jumlah_pengunjung_dewasa'] ?>
                orang
            </td>
        </tr>
        <tr>
            <td>
                Pengunjung Anak
            </td>
            <td style="width: 10px" class="text-center">:</td>
            <td>
                <?= $data['jumlah_pengunjung_anak'] ?>
                orang
            </td>
        </tr>
        <tr>
            <td>
                Harga Tiket
            </td>
            <td style="width: 10px" class="text-center">:</td>
            <td>
                Rp. <?= number_format($data['harga_tiket'], 0,',', '.')  ?>
            </td>
        </tr>
        <tr>
            <td>
                Total Bayar
            </td>
            <td style="width: 10px" class="text-center">:</td>
            <td>
                Rp. <?= number_format($data['total_bayar'], 0,',', '.')  ?>
            </td>
        </tr>
    </table>

    <br>

    <a href="<?= URL ?>" type="button" class="btn btn-primary">
        Selesai
    </a>

</div>

<!-- Mengambil komponen footer -->
<?php load_component('footer'); ?>