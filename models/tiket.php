<?php
// Memasukan file konfigurasi
include_once dirname(__DIR__) . '/config.php';

/**
 * File ini berisi fungsi untuk berkomunikasi dengan database
 */

 // Koneksi ke database
function connect()
{
    // Koneksi ke database
    $mysqli = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);

    // Check connection
    if ($mysqli -> connect_errno) {
        throw new Exception("Database error : " . $mysqli -> connect_error);
    }

    // Mengembalikan nilai koneksi
    return $mysqli;
}

// Fungsi untuk mengambil data wisata dari database
function get_wisata()
{
    // Query sql
    $sql = "SELECT * FROM `wisata`";

    // Koneksi ke database
    $mysqli = connect();
    
    // Eksekusi query
    $result = $mysqli -> query($sql);

    // Ambil data dari hasil query menjadi array
    $hasil = $result -> fetch_all(MYSQLI_ASSOC);

    // fungsi membebaskan memori yang terkait dengan hasil.
    $result -> free_result();

    // Tutup koneksi
    $mysqli -> close();

    return $hasil;
}

function get_pengunjung_museum()
{
    // Query sql
    $sql = "SELECT (SUM(jumlah_pengunjung_dewasa)+SUM(jumlah_pengunjung_anak)) as pengunjung_museum FROM pemesanan WHERE wisata = 1;";

    // Koneksi ke database
    $mysqli = connect();
    
    // Eksekusi query
    $result = $mysqli -> query($sql);

    // Ambil data dari hasil query menjadi array
    $hasil = $result -> fetch_all(MYSQLI_ASSOC);

    // fungsi membebaskan memori yang terkait dengan hasil.
    $result -> free_result();

    // Tutup koneksi
    $mysqli -> close();

    return $hasil[0]['pengunjung_museum'];
}

function get_pengunjung_pantai()
{
    // Query sql
    $sql = "SELECT (SUM(jumlah_pengunjung_dewasa)+SUM(jumlah_pengunjung_anak)) as pengunjung_pantai FROM pemesanan WHERE wisata = 2;";

    // Koneksi ke database
    $mysqli = connect();
    
    // Eksekusi query
    $result = $mysqli -> query($sql);

    // Ambil data dari hasil query menjadi array
    $hasil = $result -> fetch_all(MYSQLI_ASSOC);

    // fungsi membebaskan memori yang terkait dengan hasil.
    $result -> free_result();

    // Tutup koneksi
    $mysqli -> close();

    return $hasil[0]['pengunjung_pantai'];
}

function get_pengunjung_taman()
{
    // Query sql
    $sql = "SELECT (SUM(jumlah_pengunjung_dewasa)+SUM(jumlah_pengunjung_anak)) as pengunjung_taman FROM pemesanan WHERE wisata = 3;";

    // Koneksi ke database
    $mysqli = connect();
    
    // Eksekusi query
    $result = $mysqli -> query($sql);

    // Ambil data dari hasil query menjadi array
    $hasil = $result -> fetch_all(MYSQLI_ASSOC);

    // fungsi membebaskan memori yang terkait dengan hasil.
    $result -> free_result();

    // Tutup koneksi
    $mysqli -> close();

    return $hasil[0]['pengunjung_taman'];
}

// Mengumpulkan nama kelas dan diurutkan berdasarkan abjad
function nama_kelas()
{
    // Mengambil data wisata dari database
    $wisata = get_wisata();
    
    // Mengambil nama kelas dari data wisata
    $nama_kelas = array_column($wisata, 'nama', 'id');
    
    // Mengurutkan nama kelas berdasarkan abjad
    asort($nama_kelas);
    
    // Mengembalikan nilai nama kelas
    return $nama_kelas;
}

// Menambahkan data ke database
function save_pemesanan(
    $nama_lengkap,
    $nomor_identitas,
    $nomor_hp,
    $wisata,
    $tgl_kunjungan,
    $jumlah_pengunjung_dewasa,
    $jumlah_pengunjung_anak,
    $harga_tiket,
    $total_bayar
)
{
    $sql = "INSERT INTO `pemesanan`(`nama_lengkap`, `nomor_identitas`, `nomor_hp`, `wisata`, `tgl_kunjungan`, `jumlah_pengunjung_dewasa`, `jumlah_pengunjung_anak`, `harga_tiket`, `total_bayar`) VALUES
    ('$nama_lengkap','$nomor_identitas','$nomor_hp','$wisata','$tgl_kunjungan','$jumlah_pengunjung_dewasa','$jumlah_pengunjung_anak', $harga_tiket, '$total_bayar')";

    // execute query
    $mysqli = connect();

    $result = $mysqli -> query($sql);

    // tutup koneksi
    $mysqli -> close();

    return $result;
}

// Menampilkan isi tabel wisata berdasarkan id
function wisata_by_id($id)
{
    $sql = "SELECT * FROM `wisata` WHERE `id` = $id";

    $mysqli = connect();

    $result = $mysqli -> query($sql);

    $hasil = $result -> fetch_assoc();

    $result -> free_result();

    $mysqli -> close();

    return $hasil['nama'];
}